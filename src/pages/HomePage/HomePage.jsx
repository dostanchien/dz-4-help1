//import {useLocation} from  "react-router-dom"
import {useEffect} from  "react";
import {useDispatch,useSelector} from "react-redux";
//import {actionAddToFavorite, actionAddToProducts,actionProducts} from "../../store/actions.js";
import {selectorProducts} from "../../store/selectors.js";
import { actionFetchProducts } from "../../store/productsSlice.js";
import PropTypes from 'prop-types';
import CardList from "../../components/Card/Components/CardList";
const HomePage = ({favorite, date, handleModal, handleCurrentPost, onClickIcon}) => {
//const location = useLocation() /* useLocation хук нам дає обьект в якому індефикується кожна сторінка   */



const prd = useSelector(selectorProducts);
const dispatch = useDispatch();

useEffect(() => {
	dispatch(actionFetchProducts(prd))

}, [])

{console.log('prd',prd)}
	//console.log('app',app);



	return (
		<>
			<h2>Керамічні обігрівачі</h2>

			<p>-----------------------------------</p>
			<CardList favorite={favorite} date={date} handleModal={handleModal} handleCurrentPost={handleCurrentPost}
				onClickIcon={onClickIcon}></CardList>

		</>
	)
}

HomePage.defaultProps = {
	onClickIcon: () => {},
	handleModal: () => {},
	handleCurrentPost: () => {}
  }
  HomePage.propTypes = {
	  favorite: PropTypes.array,
	  date: PropTypes.array,
	handleModal: PropTypes.func,
	handleCurrentPost: PropTypes.func,
	onClickIcon: PropTypes.func,
	children:PropTypes.any
  }
export default HomePage