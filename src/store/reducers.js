import {createReducer} from "@reduxjs/toolkit";
import * as actions from "./actions.js";

const initialState = {
	favorites: [],
	carts: [],
	currentPost: {},
	products: [],
	isModalImage: false
}

export default createReducer(initialState, {
	[actions.actionAddToFavorite]: (state, {payload}) => { /// action === {} => {type, payload}
		// const {titel} = payload
		state.favorites = [...state.favorites, payload]
	},
	[actions.actionAddToCarts]: (state, {payload}) => { 
		state.carts = [...state.carts, payload]
	},
	[actions.actionIsModalImage]: (state) => { 
		state.isModalImage=!state.isModalImage
	},
	[actions.actionCurrentPost]: (state, {payload}) => {
		state.currentPost = payload
	},
	[actions.actionAddToProducts]: (state, {payload}) => { 
		state.products = payload
	}
})