import {createAction} from "@reduxjs/toolkit";
import {sendRequest} from '../helpers/sendRequest'

export const actionAddToFavorite = createAction("ACTION_ADD_TO_FAVORITE")
export const actionAddToCarts = createAction("ACTION_ADD_TO_CARTS")
export const actionIsModalImage = createAction("ACTION_ISMODALIMAGE")
export const actionCurrentPost = createAction("ACTION_CURRENTPOST")

export const actionAddToProducts = createAction("ACTION_ADD_TO_PRODUCTS")

export const actionProducts = (arr)=> (dispatch) => {
	
			dispatch(actionAddToProducts(arr))
}
