import {createSlice} from "@reduxjs/toolkit";
import { sendRequest } from "../helpers/sendRequest";

const productsSlice = createSlice({
    name: "product",
    initialState: {
       products: [],
       isLoading: true
    },
    reducers: {
        actionAddToProducts: (state, {payload}) => {
            state.products = [...payload]
        },
        actionLoading: (state,{payload}) => {
            state.isLoading = payload
        }
    }
})

export const {actionAddToProducts,actionLoading} = productsSlice.actions;

export const actionFetchProducts = () => (dispatch) => {
    dispatch(actionLoading(true));
    return sendRequest(window.location.origin +'/products.json')
    .then((data) => { 
        console.log('results',data);
        dispatch(actionAddToProducts(data));
        dispatch(actionLoading(false));
        console.log('actionLoading',1);
    })
}
export default productsSlice.reducer;