import PropTypes from 'prop-types'
import './Modal.scss'


const ModalWrapper = ({click, children, isOpen}) =>{
    return(
        <>
        {isOpen && (<div className="modal-wrapper" onClick={click}>{children}</div>)}
        </>
    )
}

ModalWrapper.defaultProps = {
    click: PropTypes.func,
    isOpen: false
}

ModalWrapper.propTypes = {
    click: PropTypes.func,
    children: PropTypes.any,
    isOpen: PropTypes.bool
}
export default ModalWrapper