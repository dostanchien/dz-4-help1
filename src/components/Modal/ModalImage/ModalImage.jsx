import PropTypes, { array, object } from 'prop-types'
import styled from "styled-components"
import ModalWrapper from "../ModalWrapper"
import Modal from "../Modal"
import ModalHeader from "../ModalHeader"
import ModalBody from "../ModalBody"
import ModalFooter from "../ModalFooter"
import ModalClose from "../ModalClose"
import '../Modal.scss'
const PriceSt =styled.span`
  font-size:150%;
  text-align: center;
  font-weight:400;
  color:  darkgreen;
  padding-top: 1rem;
  font-style:italic;
  `;
 const ArticleSt=styled.p`
 text-align: right;
 font-weight:200;
 font-style:italic;
 `;

const ModalImage = ({cart,products, handleOk, handleClose, isOpen}) =>{
    const handleOutside = (event) => {
        if(!event.target.closest(".modal")){
                handleClose()
        }
    }
    const isAdded=cart.some((favor) => favor.article === products.article)//меняем цвета иконок фаворите=проверка
    return(
        <ModalWrapper  className="modal-wrapper" click={handleOutside} isOpen={isOpen}>
            <Modal>
                <ModalClose click={handleClose}/>
                <ModalHeader>
                    <figure>              
                        <img className="card-img-top" src={products.images} alt={products.name} />
                    </figure>
                </ModalHeader>
                <ModalBody>
                    <ArticleSt>{'Артикул: '+ products.article}</ArticleSt>
                    <h3>{products.name}</h3>
                    <p><i>{products.color}</i></p>        
                    <PriceSt>{products.price} грн.</PriceSt>
                </ModalBody>
                <ModalFooter textFirst={!isAdded ? "Додати до кошику" : "Видалити з кошика"}
                                clickFirst={() => {handleOk();handleClose();}}
                                textSecondary="Закрити"  clickSecondary={handleClose}
                                />
            </Modal>
        </ModalWrapper>
    )
}
ModalImage.defaultProps = {
    handleOk: () => {},
    handleClose: () => {},
  }
ModalImage.propTypes = {
    cart: PropTypes.array,
    products: PropTypes.object,
    handleOk: PropTypes.func,
    handleClose: PropTypes.func,
    isOpen: PropTypes.bool
}

export default ModalImage