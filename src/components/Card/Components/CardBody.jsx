import PropTypes from 'prop-types'
import styled from 'styled-components';

const BodySt = styled.div`
-ms-flex: 1 1 auto;
flex: 1 1 auto;
min-height: 1px;
padding: 0.5rem 1rem;
`;

const CardBody = ({children})=>{
    return(
        <BodySt>{children}</BodySt>
    )
}

CardBody.propTypes = {
    children: PropTypes.any
}

export default CardBody