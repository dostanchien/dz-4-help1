import PropTypes from 'prop-types'
import Card from './Card';

const CardList = ({favorite, date, handleModal,handleCurrentPost,children,onClickIcon})  =>{
   
	const ProductsItems = date.map((item, index) => 
  ( 
  <Card   products={item} key={index}
          isAdded={favorite.some((favor) => favor.article === item.article)}//меняем цвета иконок фаворите=проверка
          handleModal={handleModal}
          handleCurrentPost={handleCurrentPost}
          onClickIcon={onClickIcon}
         >
    {children}
  </Card>))
  return(    
      <>{ProductsItems}</>
  ) 
}


CardList.defaultProps = {
	onClickIcon: () => {},
	handleModal: () => {},
	handleCurrentPost: () => {}
  }
  CardList.propTypes = {
	  favorite: PropTypes.array,
	  date: PropTypes.array,
	handleModal: PropTypes.func,
	handleCurrentPost: PropTypes.func,
	onClickIcon: PropTypes.func,
	children:PropTypes.any
  }
export default CardList
