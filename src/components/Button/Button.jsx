import cx from "classnames"
import PropTypes from 'prop-types'
import {Link} from "react-router-dom";
import './Button.scss';

// const ButtonSt = styled.button`
//     background: ${(props) => props.ver === "promery"?"#000":"red"};

//     &.btn-red {
//       background: red
//     }
//   `
  // 1)func = <button class="sjfhdfs">
  // 1)css selector = <button class="sjfhdfs btn-red">



const Button = (props) => {
  const {type, classNames, boxView, underlineView, children, click, to, href, ...restProps} = props
  ///...restProps == {.......}

  let Component = href ? 'a' : 'button'

  if(to){
    Component = Link;
  }

  return (
    <Component 
    onClick={click} 
      className={cx(
        "button",
        classNames, 
        {_box:boxView}, 
        {"_outline":underlineView})
      } 
      type={(!href && !to) && type} 
      to={to}
      href={href}
      {...restProps}>
        {children}
    </Component>
  )
}
Button.defaultProps = {
  type: "button",
  click: () => {}
}

Button.propTypes = {
  type: PropTypes.string,
  to: PropTypes.string,
  href: PropTypes.string,
  classNames: PropTypes.string, 
  boxView: PropTypes.bool,
  underlineView: PropTypes.bool,
  children: PropTypes.any,
  click: PropTypes.func
}

export default Button
